package entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@Setter
@ToString
public class Employee {

    private final String name;
    private final String jobTitle;
    private final String email;
    private final String phone;
    private final int age;

}
