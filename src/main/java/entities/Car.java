package entities;

public class Car {

    public void start() {
        startElectricity();
        startCommand();
        startFuelSystem();
    }

    private void startElectricity() {
        System.out.println("Starting electricity of car");
    }

    private void startCommand() {
        System.out.println("Starting command panel");
    }

    private void startFuelSystem() {
        System.out.println("Starting fuel system");
    }
}
