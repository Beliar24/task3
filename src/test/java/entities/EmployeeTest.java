package entities;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;

class EmployeeTest {

    Employee employee =
            new Employee("Den", "Developer", "DenDeveloper@gmail.com", "+380637777777", 25);

    @Test
    void shouldBeCreatedObject() {

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(employee.getAge()).isEqualTo(25);
        softly.assertThat(employee.getName()).isEqualTo("Den");
        softly.assertThat(employee.getJobTitle()).isEqualTo("Developer");
        softly.assertThat(employee.getEmail()).isEqualTo("DenDeveloper@gmail.com");
        softly.assertThat(employee.getPhone()).isEqualTo("+380637777777");
        softly.assertAll();
    }

}